package com.app.move.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.app.move.model.Acceleration;
import com.app.move.model.Motion;

import java.util.ArrayList;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class MoveDAO {
    private static final String MOTION_TABLE_NAME = "MOTION";
    private static final String[] MOTION_COLUMNS = {"_ID", "MOVE", "ACTION"};

    private SQLiteDatabase db;
    private Context context;

    public MoveDAO(Context context) {
        DBCore dbCore = new DBCore(context);
        this.context = context;
        db = dbCore.getWritableDatabase();
    }

    public void insertToMotion(Motion move)
    {
        ContentValues values = new ContentValues();
        values.put(MOTION_COLUMNS[1],move.getPoints().toString());
        values.put(MOTION_COLUMNS[2], move.getAction());
        db.insert(MOTION_TABLE_NAME,null, values);
    }

    public ArrayList<Motion> getAllMoves()
    {
        ArrayList<Motion> result = new ArrayList<>();
        Cursor move_cursor = db.query(MOTION_TABLE_NAME, MOTION_COLUMNS, null, null, null, null, "_ID ASC");

        if(move_cursor.getCount() > 0)
        {
            move_cursor.moveToFirst();
            do
            {
                ArrayList<Acceleration> accelerations = stringToArrayAccel(move_cursor.getString(1));
                Motion moveModel = new Motion(move_cursor.getInt(0), accelerations, move_cursor.getString(2));

                result.add(moveModel);
            }while(move_cursor.moveToNext());
        }

        return result;
    }

    public void removeMotion(Motion moveModel)
    {
        db.delete(MOTION_TABLE_NAME,"_ID = " + moveModel.getId(),null);
    }

    public ArrayList<Acceleration> stringToArrayAccel(String s)
    {
        ArrayList<Acceleration> result = new ArrayList<>();
        String[] setOfAccelerations = s.split(",");
        for(String spoint: setOfAccelerations)
        {
            String[] point = spoint.split(";");
            Acceleration p = new Acceleration(Float.parseFloat(point[0]),Float.parseFloat(point[0]),Float.parseFloat(point[0]));
            result.add(p);
        }
        return result;
    }


}