package com.app.move.model;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class Acceleration {
    private float posY;
    private float posX;
    private float posZ;

    private static final int erro = 1;

    public Acceleration(float posY, float posX, float posZ)
    {
        this.posY = posY;
        this.posX = posX;
        this.posZ = posZ;
    }

    public Acceleration(String point)
    {
        String [] vet = point.split(";");
        posX = Float.parseFloat(vet[0]);
        posY = Float.parseFloat(vet[1]);
        posZ = Float.parseFloat(vet[2]);
    }

    public float getPosY()
    {
        return posY;
    }


    public float getPosZ()
    {
        return posZ;
    }


    public float getPosX()
    {
        return posX;
    }

    @Override
    public String toString()
    {
        return posX+";"+posY+";"+posZ;
    }

    @Override
    public boolean equals(Object o)
    {
        if(o != null)
        {
            if (o instanceof Acceleration)
            {
                Acceleration p = (Acceleration) o;
                float absPosition = Math.abs(p.getPosX() - posX);
                if (absPosition >= 0 && absPosition <= erro)
                {
                    absPosition = Math.abs(p.getPosY() - posY);
                    if (absPosition >= 0 && absPosition <= erro)
                    {
                        absPosition = Math.abs(p.getPosZ() - posZ);
                        return (absPosition >= 0 && absPosition <= erro);
                    }
                }
            }
        }
        return false;
    }
}
