package com.app.move.model;

import android.app.Service;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.IBinder;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class MotionDetectionService extends Service implements SensorEventListener {
    private SensorManager sensorManager = null;
    private Sensor accelerometer;

    @Override
    public void onCreate()
    {
        super.onCreate();
        initializeService();
    }

    private void initializeService()
    {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        registerSensorListener();
    }

    private void registerSensorListener()
    {
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void unregisterSensorListener()
    {
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
