package com.app.move.model;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class Motion { private Integer id;
    private List<Acceleration> accelerations;
    private String action;

    public Motion(List<Acceleration> points)
    {
        this.accelerations = points;
    }

    public Motion(int id, List<Acceleration> points, String action)
    {
        this.id = id;
        this.action = action;
        this.accelerations = points;
    }

    public Motion(String point)
    {
        String[] vet = point.split(",");
        accelerations = new ArrayList<Acceleration>();
        for (int i = 0; i < vet.length ; i++)
        {
            accelerations.add(new Acceleration(vet[i]));
        }
    }

    public Integer getId()
    {
        return this.id;
    }

    public List<Acceleration> getPoints()
    {
        return this.accelerations;
    }

    public String getAction()
    {
        return this.action;
    }

    @Override
    public String toString()
    {
        String retorno = "";
        for(Acceleration p : accelerations)
        {
            retorno += p.toString()+ "," ;
        }
        return retorno;
    }

    @Override
    public boolean equals(Object o)
    {
        if (o != null)
        {
            if (o instanceof Motion)
            {
                Motion m = (Motion) o;
                if(m.accelerations.size() ==  accelerations.size())
                {
                    for(int i = 0; i < accelerations.size(); i++)
                    {
                        if (accelerations.get(i).equals(m.accelerations.get(i)))
                            return false;
                    }
                    return true;
                }
            }
        }
        return  false;
    }
}