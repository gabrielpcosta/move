package com.app.move.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class DBCore extends SQLiteOpenHelper {
    private static final String BD_NAME = "MoveDB";
    private static final int BD_VERSION = 6;

    private static final String MOTION_TABLE = "CREATE TABLE MOTION(_ID INTEGER PRIMARY KEY AUTOINCREMENT, "
            + "MOVE STRING NOT NULL, ACTION STRING NOT NULL);";

    private static final String DROP_MOTION_TABLE= "DROP TABLE MOTION;";


    public DBCore(Context context)
    {
        super(context, BD_NAME, null, BD_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase bd){
        bd.execSQL(MOTION_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase bd, int arg1, int arg2) {
        bd.execSQL(DROP_MOTION_TABLE);
        onCreate(bd);
    }
}