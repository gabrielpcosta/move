package com.app.move.ui.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.app.move.model.Motion;

import java.util.ArrayList;

/**
 * Created by GabrielPereira on 05/05/2015.
 */
public class RegisteredMotionAdapter extends BaseAdapter {
    private ArrayList<Motion> motions = null;
    private Context context = null;

    public RegisteredMotionAdapter(Context context, ArrayList<Motion> motions)
    {
        this.context = context;
        this.motions = motions;
    }

    @Override
    public int getCount() {
        return motions.size();
    }

    @Override
    public Object getItem(int position) {
        return motions.get(position);
    }

    @Override
    public long getItemId(int position) {
        return motions.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return null;
    }
}
